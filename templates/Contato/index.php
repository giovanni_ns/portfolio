<?php
$form = $this->Form;
$html = $this->Html;

$linkOptions = ['escape' => false, 'target' => '_blank'];
?>

<style>
    :root {
        --animate-delay: 0.5s;
    }
</style>

<div class="container">
    <h1 class="is-size-1">Contato</h1>

    <div class="columns block">
        <div class="column is-8">
            <div class="content animate__animated animate__fadeIn animate__fast">
                <div class="container">
                    <p>
                        Para entrar em contato comigo para solicitar um orçamento para o seu aplicativo, site ou contratar meus serviços, basta preencher o formulário abaixo ou me enviar uma mensagem pelo LinkedIn. Farei o possível para responder o mais rápido possível. </p>
                    <?= $html->link('<i class="fa-brands fa-linkedin"></i> LinkedIn', 'https://linkedin.com/in/giovanni-neves-sadauscas', $linkOptions) ?>
                </div>
            </div>
        </div>
    </div>

    <div class="columns block">
        <div class="column is-6">
            <div class="column">
                <!-- FORM INICIO -->
                <?= $this->Flash->render() ?>
                <?= $form->create($contato, ['type' => 'post']) ?>
                <div class="field is-horizontal">
                    <div class="field-body animate__animated animate__fadeIn animate__slow">
                        <div class="field">
                            <p class="control is-expanded has-icons-left">
                                <?= $form->text('nome', ['class' => 'input', 'placeholder' => 'Nome completo']) ?>
                                <span class="icon is-small is-left">
                                    <i class="fas fa-user"></i>
                                </span>
                            </p>
                        </div>
                        <div class="field">
                            <p class="control is-expanded has-icons-left">
                                <?= $form->email('email', ['class' => 'input', 'placeholder' => 'Email']) ?>
                                <span class="icon is-small is-left">
                                    <i class="fas fa-envelope"></i>
                                </span>
                            </p>
                        </div>
                    </div>
                </div>
                <div class="field animate__animated animate__fadeIn animate__slow">
                    <p class="control block is-expanded has-icons-right">
                        <?= $form->textarea('detalhes', ['class' => 'textarea', 'placeholder' => 'Escreva os detalhes do contato']) ?>
                        <span class="icon is-small is-right">
                            <i class="fas fa-comment"></i>
                        </span>
                    </p>
                    <?= $form->submit('Enviar', ['class' => 'button is-success']) ?>
                </div>

                <?= $form->end() ?>
                <!-- FORM FIM -->
            </div>
        </div>
    </div>
</div>