<!DOCTYPE html>
<html lang="en">

<!-- .------..------..------. -->
<!-- |G.--. ||N.--. ||S.--. | -->
<!-- | :/\: || :(): || :/\: | -->
<!-- | :\/: || ()() || :\/: | -->
<!-- | '--'G|| '--'N|| '--'S| -->
<!-- `------'`------'`------' -->

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <?= $this->Html->meta('favicon.ico', '/favicon.ico', ['type' => 'icon']) ?>
    <?= $this->Html->meta('keywords', 'análise e desenvolvimento de sistemas, PHP, Javascript, Typescript, Ruby, Java, Laravel, Symfony, PostgreSQL, MongoDB, ReactJS, Livewire, Stimulus, Angular, Ruby on Rails, Spring boot, CodeIgniter4, suporte, consultoria, treinamento, MEDDIC, desenvolvimento de sistemas comerciais, gerenciamento de estoque, controle de vendas, cadastro de clientes, manutenção de infraestrutura de rede e computadores, SymfonyCast, Harmonious Development, freeCodeCamp, Jornada do Dev, Curso de Laravel, Curso de CodeIgniter4, Curso completo de PHP 7, Curso de PHP Básico e PHP POO') ?>
    <?= $this->Html->meta('description', 'Giovanni Neves Sadauscas é um desenvolvedor de sistemas com experiência em linguagens como PHP, Javascript, Typescript, Ruby e Java. Ele tem trabalhado com frameworks como Laravel, Symfony, ReactJS, Livewire, Stimulus e Angular, bem como bancos de dados como PostgreSQL e MongoDB. Além disso, Giovanni tem experiência em suporte, consultoria e treinamento em ferramentas como o PTC Windchill. Ele também tem experiência em desenvolvimento de sistemas comerciais, gerenciamento de estoque, controle de vendas e cadastro de clientes, bem como manutenção de infraestrutura de rede e computadores. Ele possui certificações de SymfonyCast, Harmonious Development, freeCodeCamp, Jornada do Dev, Curso de Laravel, Curso de CodeIgniter4, Curso completo de PHP 7 e Curso de PHP Básico e PHP POO.') ?>

    <?= $this->Html->css(['bulma.min', 'animate.min', 'https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.2.1/css/all.min.css']) ?>
    <?= $this->Html->script(['jquery.min']) ?>

    <title>
        <?= h($this->fetch('title')) ?> | Giovanni Neves Sadauscas
    </title>
</head>

<body>
    <!-- HEADER INICIO -->
    <header>
        <!-- NAVBAR INICIO -->
        <nav class="navbar" role="navigation" aria-label="main navigation">
            <div class="container">
                <div class="navbar-brand">

                    <a role="button" class="navbar-burger" aria-label="menu" aria-expanded="false" data-target="navbarBasicExample">
                        <span aria-hidden="true"></span>
                        <span aria-hidden="true"></span>
                        <span aria-hidden="true"></span>
                    </a>
                </div>

                <div id="navbarBasicExample" class="navbar-menu">
                    <!-- LINKS DO NAVBAR INICIO -->
                    <div class="navbar-start">

                        <?= $this->Html->link('<i class="fa-solid fa-house p-1"></i>Home', '/', ['class' => 'navbar-item', 'escape' => false]) ?>
                        <?= $this->Html->link('<i class="fa-solid fa-address-card p-1"></i>Sobre mim', '/sobremim', ['class' => 'navbar-item', 'escape' => false]) ?>
                        <?= $this->Html->link('<i class="fa-solid fa-briefcase p-1"></i>Serviços', '/servicos', ['class' => 'navbar-item', 'escape' => false]) ?>
                        <?= $this->Html->link('<i class="fa-solid fa-envelope p-1"></i>Contato', '/contato', ['class' => 'navbar-item', 'escape' => false]) ?>

                    </div>
                    <!-- LINKS DO NAVBAR FIM -->
                </div>
            </div>
        </nav>
        <!-- NAVBAR FIM -->
    </header>
    <!-- HEADER FIM -->
    <!-- MAIN INICIO -->
    <main>
        <?= $this->fetch('content') ?>
    </main>
    <!-- MAIN FIM -->
    <!-- FOOTER INICIO -->
    <footer class="footer mt-3">
        <div class="columns">
            <div class="column is-two-thirds">
                <div class="content has-text-centered">

                </div>
            </div>
            <div class="column">
                <div class="content">
                    <h3><i class="fa-solid fa-share"></i> Social</h3>
                    <ul>
                        <li><a href="https://www.linkedin.com/in/giovanni-neves-sadauscas" target="_blank"><i class="fa-brands fa-linkedin p-1"></i>LinkedIn</a></li>
                        <li><a href="https://gitlab.com/giovannins" target="_blank"><i class="fa-brands fa-gitlab p-1"></i>Gitlab</a></li>
                        <!-- <li><a href="https://www.youtube.com/@bonk0041" target="_blank"><i class="fa-brands fa-youtube p-1"></i>Youtube</a></li>
                        <li><a href="http://tiktok.com/@bonk0041" target="_blank"><i class="fa-brands fa-tiktok p-1"></i>TikTok</a></li> -->
                    </ul>
                </div>
            </div>
        </div>
    </footer>
    <!-- FOOTER FIM -->
    <!-- NAVBAR SCRIPT INICIO -->
    <script>
        $(".navbar-burger").click(function() {
            $(".navbar-burger").toggleClass("is-active");
            $(".navbar-menu").toggleClass("is-active");
        });
    </script>
    <!-- NAVBAR SCRIPT FIM -->
    <?= $this->fetch('script') ?>

</body>

</html>