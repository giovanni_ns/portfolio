<?php
$this->assign('title', 'Sobre mim');

$idade = (date('m') >= 9) ? (date('Y') - 2000) : (date('Y') - 2001);
$exp = (date('m') >= 7) ? (date('Y') - 2018) : (date('Y') - 2019);
?>

<style>
    :root {
        --animate-delay: 0.5s;
    }
</style>

<div class="container">
    <h1 class="is-size-1">Sobre mim</h1>

    <!-- TILES INICIO -->
    <div class="tile is-ancestor">
        <div class="tile is-vertical is-8">
            <div class="tile">
                <div class="tile is-parent is-vertical">
                    <article
                        class="tile is-child notification is-success animate__animated animate__fadeIn animate__fast">
                        <!-- Put any content you want -->
                        <p class="title">Quem é você?</p>
                        <div class="content">
                            <p>
                                Olá, meu nome é Giovanni Neves Sadauscas, mas você pode me chamar de "Mago dos Códigos"
                                (brincadeirinha, mas seria legal, né?).
                            </p>
                            <p>
                                Tenho
                                <?= $idade ?> anos e minha paixão pela tecnologia e software é tão grande que às vezes
                                me pego
                                sonhando com linhas de código em vez de ovelhas.
                            </p>
                            <p>
                                Quando o assunto é servidores e desenvolvimento web, meus olhos brilham mais do que o
                                Triforce de Ouro em The Legend of Zelda (falando nisso, sou um fã incondicional da
                                franquia,
                                se você ainda não percebeu).
                            </p>
                            <p>
                                Enfim, é um prazer conhecê-lo(a)!
                            </p>
                        </div>
                    </article>
                    <article
                        class="tile is-child notification is-link animate__animated animate__fadeIn animate__delay-1s">
                        <p class="title">Programa desde quando?</p>
                        <div class="content">
                            <p>
                                Sou um programador nato, comecei a escrever códigos em 2014 e desde então tenho me
                                aventurado em diversas linguagens de programação, como se fossem pratos em um rodízio em
                                uma confeitaria.
                            </p>
                            <p>
                                Já experimentei sabores como Python, Ruby, C/C++, C#, Java, Emacs Lisp, Go e Lua, cada
                                uma
                                com suas peculiaridades e desafios. Foi uma jornada tão intensa que me sinto como um
                                samurai
                                que treinou em todas as artes marciais do Japão (exceto a de luta livre de sumô, essa
                                ainda
                                não experimentei).
                            </p>
                            <p>
                                Em 2018, senti que era hora de me dedicar a uma linguagem em específico e escolhi o PHP
                                como
                                meu "sabor do momento". Desde então, tenho mergulhado fundo nessa deliciosa linguagem,
                                estudando e aprendendo cada vez mais.
                            </p>
                        </div>
                    </article>
                </div>
                <div class="tile is-parent">
                    <article class="tile is-child notification is-info animate__animated animate__fadeIn">
                        <p class="title">Quais são os seus serviços?</p>
                        <div class="content">
                            <p>

                                Olá, meu caro! Eu sou um freelancer em PHP, especializado em desenvolvimento de
                                aplicações
                                web. Trabalho nessa área há mais de
                                <?= $exp ?> anos, e embora eu não me considere um profissional
                                experiente, posso garantir que sou um desenvolvedor obstinado, sempre buscando aprender
                                e me
                                desafiar para criar soluções inovadoras e eficientes para meus clientes.
                            </p>
                            <p>
                                Como desenvolvedor PHP, ofereço uma gama completa de serviços relacionados ao
                                desenvolvimento de aplicações web. Desde o desenvolvimento de sites personalizados e
                                sistemas de gerenciamento de conteúdo (CMS) até a integração de APIs, desenvolvimento de
                                sistemas de e-commerce, aplicações móveis híbridas e muito mais.
                            </p>
                            <p>
                                Tenho conhecimento em diversas áreas do desenvolvimento web, incluindo front-end (HTML,
                                CSS,
                                JavaScript) e back-end (PHP, banco de dados, servidores). Isso me permite trabalhar em
                                projetos que exigem uma ampla gama de habilidades e oferecer soluções completas para
                                meus
                                clientes.
                            </p>
                            <p>
                                Se você precisa de um desenvolvedor web eficiente e experiente, estou aqui para
                                ajudá-lo.
                                Além de criar soluções incríveis para os seus projetos, sou também um mestre em
                                otimização
                                de performance e escalabilidade de aplicações existentes, além de manutenção e
                                atualização
                                de aplicações já existentes. Se quiser saber mais sobre mim ou sobre os meus serviços,
                                entre
                                em contato! Estou sempre à disposição para novos desafios e projetos incríveis.
                            </p>

                        </div>

                    </article>
                </div>
            </div>
            <!-- <div class="tile is-parent">
                <article
                    class="tile is-child notification is-success animate__animated animate__fadeIn animate__delay-1s animate__faster">
                    <p class="title">d</p>
                </article>
            </div> -->
        </div>
        <div class="tile is-parent">
            <div class="tile is-vertical">
                <article class="tile is-child notification is-link animate__animated animate__fadeIn animate__slow">
                    <p class="title">Sem Front-end?</p>
                    <div class="content">
                        <p>
                            Apesar de ser um especialista em back-end e PHP, também dediquei um tempo para aprender
                            sobre
                            frameworks e bibliotecas front-end, como Angular, jQuery, Vue, Svelte e React, para poder
                            atender melhor aos meus clientes e oferecer soluções completas para seus projetos.
                        </p>
                        <p>
                            Embora eu não seja um designer nato, tenho conhecimentos básicos de front-end e sou capaz de
                            criar soluções incríveis que funcionam perfeitamente em todos os dispositivos. Utilizo CSS
                            pronto e, quando necessário, faço ajustes para atender às necessidades dos meus clientes.
                        </p>
                        <p>
                            Acredito que é importante ter conhecimentos básicos de front-end para entregar projetos
                            completos e bem-sucedidos. E é por isso que me esforço para aprender o máximo possível sobre
                            as
                            tecnologias front-end, mantendo-me atualizado sobre as últimas tendências e novidades.
                        </p>
                    </div>
                </article>
                <article
                    class="tile is-child notification is-primary animate__animated animate__fadeIn animate__slower">
                    <p class="title">Por que escolheu o PHP?</p>
                    <div class="content">
                        <p>
                            A escolha pelo PHP foi uma decisão estratégica e inteligente. Essa linguagem tem um
                            histórico
                            sólido e é considerada uma das mais antigas do mundo da programação. Isso significa que a
                            documentação é excepcional e o mercado tem muitos sistemas legados que precisam de
                            profissionais
                            ou de atualizações.
                        </p>
                        <p>
                            Sem falar que grandes sistemas de gerenciamento de conteúdo, como WordPress, Magento e
                            Drupal,
                            são baseados em PHP. Isso só comprova a versatilidade e a potência dessa linguagem.
                        </p>
                        <p>

                            Além disso, escolher o PHP é como ter uma Ferrari na garagem: é uma escolha clássica e
                            atemporal. Você não precisa se preocupar com a moda do momento ou com as tendências
                            passageiras.
                            PHP é um clássico que sempre estará em alta.
                        </p>
                    </div>
                </article>
            </div>
        </div>
    </div>
    <!-- TILES FIM -->
</div>