<?php
$this->assign('title', 'Página principal');
$exp = (date('m') >= 7) ? (date('Y') - 2018) : (date('Y') - 2019);

$backendList = [
    '<i class="fa-brands fa-php"></i> PHP' => [
        '<i class="fa-solid fa-fire"></i> CodeIgniter4',
        '<i class="fa-solid fa-cake-candles"></i> CakePHP',
        '<i class="fa-brands fa-symfony"></i> Symfony',
        '<i class="fa-brands fa-laravel"></i> laravel',
    ],
    $this->Html->image('icon/icons8-c-afiado-logotipo.svg', ['width' => '16px']) . 'C#' => [
        $this->Html->image('icon/icons8-asp-26.png', ['width' => '16px']) . '.NET Core',
    ],
    '<i class="fa-solid fa-database"></i> Banco de dados' => [
        $this->Html->image('icon/icons8-microsoft-sql-server.svg', ['width' => '16px']) . 'SQL Server',
        $this->Html->image('icon/icon8-postgresql.svg', ['width' => '16px']) . 'PostgreSQL',
        $this->Html->image('icon/icons8-mysql-logo.svg', ['width' => '16px']) . 'MySQL',
        $this->Html->image('icon/icons8-oracle.svg', ['width' => '16px']) . 'Oracle SQL',
        '<i class="fa-brands fa-envira"></i> MongoDB',
        $this->Html->image('icon/icons8-redis.svg', ['width' => '16px']) . 'Redis'
    ]
];

$frontendList = [
    '<i class="fa-brands fa-square-js"></i> Javascript' => [
        '<i class="fab fa-angular"></i> Angular',
        '<i class="fa-brands fa-jquery"></i> <i class="fa-solid fa-rss"></i> jQuery',
        '<i class="fa-brands fa-vuejs"></i> Vue',
        '<i class="fa-solid fa-link"></i> Svelte',
        '<i class="fa-brands fa-react"></i> ReactJS',
    ]
];

$ferramentasList = [
    '<i class="fa-brands fa-git-alt"></i> Git',
    '<i class="fa-brands fa-docker"></i> Docker',
    '<i class="fa-brands fa-linux"></i> Linux',
    '<i class="fa-solid fa-cloud"></i> Azure, AWS, Google Cloud',
];
?>

<style>
    :root {
        --animate-delay: 0.5s;
    }
    #mago:hover>p#mostra_mago::before {
        content: "O mago do código.";
        position: absolute;
        opacity: 1;
    }
</style>


<!-- HERO INICIO -->
<section class="hero block is-link animate__animated animate__fadeInLeft">
    <div class="container hero-body" id="mago">
        <p class="title">
            Giovanni Neves Sadauscas
        </p>
        <p class="subtitle animate__animated animate__fadeInLeft animate__delay-1s">
            Desenvolvedor <strong>back-end</strong> e <strong>full-stack</strong> PHP.
        </p>
        <p id="mostra_mago" class="animate__animated animate__fadeIn"></p>
    </div>
</section>
<!-- HERO FIM -->

<div class="block animate__animated animate__fadeIn animate__delay-2s">
    <div class="content">
        <div class="container">
            <h1>Olá!</h1>
            <p>
                É um prazer conhecê-los. Meu nome é <strong>Giovanni</strong> e eu sou um desenvolvedor PHP (também
                conhecido como mago do código!). Sou formado em Análise e Desenvolvimento de Sistemas, e desde então, me
                dedico ao desenvolvimento de soluções web de alta qualidade utilizando PHP.
            </p>

            <p>
                Com mais de
                <?= $exp ?> anos de experiência no mercado, eu já trabalhei em diversos projetos como freelancer, o que
                me permitiu aprimorar minhas habilidades em frameworks, práticas de código limpo e metodologias ágeis de
                desenvolvimento. Por isso, eu posso garantir soluções incríveis para os meus clientes.
            </p>

            <p>
                Além de ser um especialista em PHP, eu também sou um verdadeiro ninja em Javascript e suas principais
                frameworks e bibliotecas front-end, como Angular, jQuery, Vue, Svelte e React. Isso significa que eu sou
                capaz
                de desenvolver soluções completas e de alta qualidade tanto no back-end quanto no front-end.
            </p>
            <p>
                Em PHP, eu tenho conhecimento avançado em diversos frameworks, como Laravel, CakePHP, Symfony e
                CodeIgniter4. Eu posso dizer com segurança que sou um verdadeiro expert nesses frameworks. Por isso, se
                você precisa de uma solução completa e de alta qualidade, eu sou a pessoa certa para você!
            </p>
            <h3>Principais habilidades técnicas.</h3>
            <p>Dentre minhas principais habilidades técnicas, destaco o domínio das seguintes tecnologias e ferramentas:
            </p>
            <!-- HABILIDADES INICIO -->
            <div class="columns">
                <!-- BACK-END INICIO -->
                <div class="column">
                    <h5>Back-end</h5>

                    <?= $this->Html->nestedList($backendList) ?>

                </div>
                <!-- BACK-END FIM -->
                <!-- FRONT-END INICIO -->
                <div class="column">
                    <h5>Front-end</h5>

                    <?= $this->Html->nestedList($frontendList) ?>

                </div>
                <!-- FRONT-END FIM -->
                <div class="column">
                    <h5>Ferramentas</h5>

                    <?= $this->Html->nestedList($ferramentasList) ?>

                </div>
            </div>
            <!-- HABILIDADES FIM -->
        </div>
    </div>
</div>