<?php
$this->assign('title', '404');

?>

<section class="hero is-danger">
  <div class="hero-body">
    <p class="title">
      404
    </p>
    <p class="subtitle is-family-monospace">
      Oops...Página não encontrada.
    </p>
  </div>
</section>

<section class="container mb-5 content">
    <div class="column is-11">
        <p class="is-size-5">
            Parece que a página que você estava procurando saiu para tomar um café e não deixou nenhum aviso de volta. 
        </p>
        <p class="is-size-5">   
            Talvez ela volte em breve, mas por enquanto, que tal se distrair vendo um <a class="has-text-weight-semibold" href="https://youtu.be/dQw4w9WgXcQ" target="_blank" rel="noopener noreferrer">vídeo</a>?
        </p>
    </div>
</section>