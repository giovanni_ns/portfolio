<?php
$this->assign('title', '503');

?>

<section class="hero is-danger">
    <div class="hero-body">
        <p class="title">
            503
        </p>
        <p class="subtitle is-family-monospace">
            Oops... Não foi possível estabelecer uma conexão.
        </p>
    </div>
</section>

<section class="container mb-5 content">
    <div class="column is-11">
        <p class="is-size-5">
            Oh não! Parece que estamos com problemas para conectar o seu dispositivo com o servidor.
        </p>
        <p class="is-size-5">
            TTalvez o cabo de rede esteja jogando uma partida de esconde-esconde ou os pacotes de dados resolveram tirar
            férias. Vamos procurar a conexão perdida, mas enquanto isso, que tal dançar a <a class="has-text-weight-semibold" href="https://youtu.be/zWaymcVmJ-A" target="_blank" rel="noopener noreferrer">Macarena</a> para passar o tempo?
        </p>
    </div>
</section>