<?php
$this->assign('title', 'Serviços prestados, portfólio');

?>

<style>
    :root {
        --animate-delay: 0.5s;
    }
</style>

<div class="container">
        
</div>
<div class="container animate__animated animate__slideInUp animate__fast">
    <?php foreach ($servicos as $servico) : ?>
        <div class="card block">
            <div class="card-content">
                <div class="media">
                    <div class="media-left">
                        <figure class="image is-48x48">
                            <img src="<?= $servico['logo'] ?>">
                        </figure>
                    </div>
                    <div class="media-content">
                        <p class="title is-4"><?= $servico['titulo'] ?></p>
                        <p class="subtitle is-6"><a href="<?= $servico['url'] ?>" target="_blank" rel="noopener noreferrer"><?= $servico['empresa'] ?></a></p>
                    </div>
                </div>

                <div class="content">
                    <?= $servico['detalhes'] ?>
                </div>
                <div class="tags">
                    <?php foreach (explode(',', $servico['tecnologias']) as $tags) : ?>
                        <span class="tag"><?= $tags ?></span>
                    <?php endforeach; ?>
                </div>
            </div>
        </div>
    <?php endforeach; ?>
</div>