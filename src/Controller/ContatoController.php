<?php
declare(strict_types=1);

namespace App\Controller;

/**
 * Contato Controller
 *
 * @method \App\Model\Entity\Contato[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class ContatoController extends AppController
{
    /**
     * Index method
     *
     * @return \Cake\Http\Response|null|void Renders view
     */
    public function index()
    {
        $contato = $this->Contato->newEmptyEntity();
        if ($this->request->is('post')) {
            $contato = $this->Contato->patchEntity($contato, $this->request->getData());
            $contato->criado_em = date(DATE_RFC850);
            if ($this->Contato->save($contato)) {
                $this->Flash->success(__('Mensagem enviada com sucesso! Entrarei em contato pelo e-mail fornecido em breve.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('Oops... Algo deu errado e a mensagem não pode ser enviada, por favor, tente novamente.'));
        }
        $this->set(compact('contato'));
    }

    /**
     * View method
     *
     * @throws \Cake\Http\Response|null|void Renders view
     */
    public function view()
    {
        $contato = $this->paginate($this->Contato);

        $this->set(compact('contato'));
    }

}
