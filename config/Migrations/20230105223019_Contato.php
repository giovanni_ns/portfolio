<?php
declare(strict_types=1);

use Migrations\AbstractMigration;

class Contato extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * https://book.cakephp.org/phinx/0/en/migrations.html#the-change-method
     * @return void
     */
    public function change(): void
    {
        $table = $this->table('contato');
        $table->addColumn('nome', 'string', ['limit' => 255, 'null' => false]);
        $table->addColumn('email', 'string', ['limit' => 255, 'null' => false]);
        $table->addColumn('detalhes', 'text', ['null' => false]);
        $table->addColumn('criado_em', 'string', ['limit'=> 50, 'null' => false]);
        $table->create();
    }
}
