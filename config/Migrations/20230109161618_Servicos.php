<?php
declare(strict_types=1);

use Migrations\AbstractMigration;

class Servicos extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * https://book.cakephp.org/phinx/0/en/migrations.html#the-change-method
     * @return void
     */
    public function change(): void
    {
        $table = $this->table('servicos');
        $table->addColumn('titulo', 'string', ['limit' => 255, 'null' => false]);
        $table->addColumn('tecnologias', 'string', ['limit' => 255, 'null' => false]);
        $table->addColumn('empresa', 'string', ['limit' => 255, 'null' => false]);
        $table->addColumn('logo', 'string', ['limit' => 255, 'null' => false]);
        $table->addColumn('url', 'string', ['limit' => 255, 'null' => false]);
        $table->addColumn('detalhes', 'text', ['null' => false]);
        $table->create();
    }
}
