<?php

declare(strict_types=1);

use Migrations\AbstractSeed;

/**
 * Servicos seed.
 */
class ServicosSeed extends AbstractSeed
{
    /**
     * Run Method.
     *
     * Write your database seeder using this method.
     *
     * More information on writing seeds is available here:
     * https://book.cakephp.org/phinx/0/en/seeding.html
     *
     * @return void
     */
    public function run(): void
    {
        $datas = [
            1 => [
                'titulo' => mb_convert_encoding('Integração PWA em aplicativo Android', 'ISO-8859-1'),
                'tecnologias' => 'Java,Android,PHP,PWA,MySQL,REST API',
                'empresa' => mb_convert_encoding('Psicoplanner', 'ISO-8859-1'),
                'logo' => 'https://psicoplanner.com.br/wp-content/uploads/2022/04/logo_branco.png',
                'url' => 'https://psicoplanner.com.br',
                'detalhes' => mb_convert_encoding('<p>Implementação de um Progressive Web App (PWA) para dispositivos Android. O PWA foi desenvolvido com Java e Android Studio, e utiliza uma REST API para acessar dados de um servidor. Também fiz a reintegração de uma API PHP existente no PWA para permitir que ele acesse os dados do servidor de maneira segura.</p>
                                                    <p>O processo de implementação envolveu a criação de uma estrutura básica para o PWA com Android Studio, bem como a configuração da REST API para acessar os dados do servidor. Em seguida, integrei a API PHP existente no PWA para permitir que ele acesse os dados de maneira segura. Depois de concluir essas etapas, testei o PWA para garantir que estava funcionando corretamente.</p>
                                                    <p>No geral, foi uma ótima experiência de desenvolvimento e estou satisfeito com o resultado final do PWA para Android. A implementação da REST API e da reintegração da API PHP permitiu que o PWA acesse os dados de maneira eficiente e segura, tornando-o uma ótima opção para os usuários do Android.</p>', 'ISO-8859-1')

            ],

            2 => [
                'titulo' => 'Mini ERP',
                'tecnologias' => 'PHP,jQuery,Javascript,HTML,CSS,MySQL,Git',
                'empresa' => 'Guerreiros Games',
                'logo' => 'https://i.pinimg.com/280x280_RS/41/d5/0b/41d50b3295ad97be95db0d2f2af1bbe6.jpg',
                'url' => 'https://guerreirosgames.com.br',
                'detalhes' => mb_convert_encoding('<p>Desenvolvi um sistema de mini ERP para gerenciamento interno de ordens de serviço, estoque, controle de usuários e acesso a dados. Utilizei a linguagem de programação PHP no back-end, junto com o banco de dados MySQL para armazenar os dados. No front-end, utilizei HTML, CSS e jQuery para criar a interface do usuário e torná-la responsiva. Também utilizei JavaScript para adicionar funcionalidades dinâmicas à interface.</p>
                               <p>Para gerenciar o código-fonte e realizar o versionamento, utilizei o Git. Isso me permitiu realizar o controle de alterações no código e facilmente desenvolver novas funcionalidades.</p>
                               <p>O sistema permite que os usuários criem e acessem ordens de serviço, gerenciem o estoque e adicionem ou removam usuários com facilidade. Também implementei medidas de segurança para garantir o acesso apropriado aos dados e evitar acessos não autorizados.</p>
                               <p>No geral, foi uma ótima experiência de desenvolvimento e estou satisfeito com o resultado final do sistema de mini ERP.</p>', 'ISO-8859-1')
            ],

            3 => [
                'titulo' => mb_convert_encoding('Deploy de instância Drupal 8', 'ISO-8859-1'),
                'tecnologias' => 'PHP,Drupal,MySQL,Web services',
                'empresa' => 'Motor-Vapor',
                'logo' => '',
                'url' => 'https://motorvapor.com.br',
                'detalhes' => mb_convert_encoding('<p>Fiz o deploy de uma instância do Drupal para um cliente e configurei tudo internamente no provedor de hospedagem. O processo começou com a escolha do plano de hospedagem adequado para atender às necessidades do cliente em termos de tráfego e recursos. Em seguida, criei uma nova instância do Drupal na plataforma de hospedagem e fiz o upload dos arquivos do site para o servidor.</p>
                                                    <p>Depois disso, configurei o banco de dados para o site do Drupal, criando um novo banco de dados no provedor de hospedagem e atribuindo as credenciais de acesso ao arquivo de configuração do Drupal. Também verifiquei as configurações de PHP e Apache para garantir que estavam configuradas corretamente para o site do Drupal.</p>
                                                    <p>Por último, verifiquei o site do Drupal para garantir que tudo estava funcionando corretamente e fiz o ajuste final nas configurações do site, como o título do site e as configurações de e-mail. Depois de concluir todas essas etapas, o site do Drupal estava pronto para ser usado pelo cliente.</p>', 'ISO-8859-1')
            ],

            4 => [
                'titulo' => mb_convert_encoding('Integração de API', 'ISO-8859-1'),
                'tecnologias' => 'PHP,jQuery,Javascript,MySQL,REST API,HTML,CSS',
                'empresa' => 'Sinais Power',
                'logo' => 'https://media.licdn.com/dms/image/C4D2DAQFPalSXPsqYxQ/profile-treasury-image-shrink_1920_1920/0/1668427904300?e=1673895600&v=beta&t=iNgbqP13Od3iM0CxAFh2oOdd8qKQq2GkrQN73KgN9xU',
                'url' => 'http://sinaispower.com.br',
                'detalhes' => mb_convert_encoding('<p>A integração de uma API da bolsa de valores em um sistema de sinal de daytrade que estava desenvolvendo. O objetivo era permitir que o sistema acesse dados em tempo real da bolsa de valores e os armazenasse em um banco de dados MySQL para análise posterior.</p>
                                                    <p>Para realizar a integração, utilizei a linguagem de programação PHP no back-end e o Git para controlar o código-fonte. Também utilizei o MySQL como banco de dados para armazenar o histórico de dados da bolsa de valores. No front-end, utilizei HTML, CSS e jQuery para criar a interface do usuário e torná-la responsiva, e JavaScript para adicionar funcionalidades dinâmicas à interface.</p>
                                                    <p>O processo de integração envolveu a criação de uma conexão segura com a API da bolsa de valores e o desenvolvimento de um script para coletar os dados em tempo real e armazená-los no banco de dados. Depois de concluir a integração e realizar alguns testes, o sistema de sinal de daytrade estava pronto para ser usado pelos usuários.</p>', 'ISO-8859-1')

            ],
            5 => [
                'titulo' => mb_convert_encoding('Criação de site comercial', 'ISO-8859-1'),
                'tecnologias' => 'PHP,CodeIgniter 4,jQuery,Javascript,MySQL,HTML,CSS,Bootstrap 5',
                'empresa' => mb_convert_encoding('Pizzas Pré', 'ISO-8859-1'),
                'logo' => 'http://massa.coz.br/imgs/Logo.svg',
                'url' => 'http://massa.coz.br',
                'detalhes' => mb_convert_encoding('<p>Recentemente, criei um site comercial e fiz o host dele em um provedor de hospedagem. O site possui uma vitrine de produtos que é atualizada em tempo real, permitindo que os usuários vejam os produtos mais recentes assim que eles são adicionados.</p>
                                                    <p>Para criar o site, utilizei o CodeIgniter4 como framework PHP, junto com o MySQL como banco de dados. Também utilizei o Git para controlar o código-fonte e facilitar o desenvolvimento em equipe. No front-end, utilizei HTML, CSS e o framework Bootstrap 5 para criar a interface do usuário e torná-la responsiva.</p>
                                                    <p>Depois de concluir o desenvolvimento do site, fiz o host dele no provedor de hospedagem escolhido. Isso envolveu a criação de uma conta no provedor e o upload dos arquivos do site para os servidores do provedor. Também configurei o domínio do site para apontar para os servidores do provedor e realizar o teste final para garantir que tudo estava funcionando corretamente.</p>
                                                    <p>No geral, foi uma ótima experiência de desenvolvimento e estou satisfeito com o resultado final do site comercial.</p>  ', 'ISO-8859-1')

            ],
            6 => [
                'titulo' => mb_convert_encoding('Desenvolvimento de API', 'ISO-8859-1'),
                'tecnologias' => 'C#,.NET Core,REST API,SQL Server,Web Service',
                'empresa' => mb_convert_encoding('Cruz Azul SP', 'ISO-8859-1'),
                'logo' => 'https://www.cruzazulsp.com.br/wp-content/themes/cruzazul-docpix/assets/img/logo_mobile.png',
                'url' => 'https://www.cruzazulsp.com.br/',
                'detalhes' => mb_convert_encoding('<p>Eu desenvolvi uma API para o Hospital Cruz Azul de São Paulo que permite a verificação de beneficiários e dependentes de policiais militares. Essa API foi criada para melhorar o processo de verificação de elegibilidade de atendimento aos pacientes do hospital.</p>
                                                    <p>A API foi desenvolvida com tecnologias modernas e seguras, utilizando o padrão RESTful para comunicação. Além disso, a API possui uma documentação clara e concisa, que facilita a integração com outros sistemas.</p>
                                                    <p>A API realiza a verificação de elegibilidade de forma eficiente e precisa, permitindo que o hospital identifique rapidamente se o paciente é um beneficiário ou dependente de um policial militar e, assim, possa oferecer o atendimento adequado.</p>
                                                    <p>Além disso, a API permite que o hospital mantenha um registro atualizado de todos os beneficiários e dependentes de policiais militares, tornando o processo de verificação mais ágil e seguro.</p>
                                                    <p>O desenvolvimento dessa API foi um desafio interessante e gratificante, pois pude contribuir para a melhoria dos serviços oferecidos pelo Hospital Cruz Azul de São Paulo. Estou orgulhoso de ter desenvolvido uma solução que beneficia diretamente aqueles que se dedicam à proteção da sociedade.</p> ', 'ISO-8859-1')

            ],
        ];

        $table = $this->table('servicos');
        foreach ($datas as $data) {
            $table->insert($data)->save();
        }
    }
}